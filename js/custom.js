$(window).load(function(){
	var mobile   = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);
	//var hendlerTouch = mobile ? "touchend" : "onmousedown";

	if( mobile ) {
		$('body').addClass('ios');
	} else{
		$('body').addClass('web');
	};
	$('body').removeClass('loaded');
});

/* viewport width */
function viewport(){
	var e = window, 
	a = 'inner';
	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
};
/* viewport width */

$(function(){
	/* placeholder */	   
	$('input, textarea').each(function(){
		var placeholder = $(this).attr('placeholder');
		$(this).focus(function(){ $(this).attr('placeholder', '');});
		$(this).focusout(function(){			 
			$(this).attr('placeholder', placeholder);  			
		});
	});
	/* placeholder */


	/* form styler */
	if($('.styled').length) {
		$('.styled').styler();
	};
	/* form styler */


	/* owl-carousel*/
	if($('.owl-carousel').length){
		var owl = $('.owl-carousel');
		owl.owlCarousel({
			items: 1,
			loop: true,
			autoplay: true,
			autoplayTimeout: 4000,
			autoplaySpeed: 600,
			dragEndSpeed: 600,
			nav: false
		});		
	};
	/* owl-carousel*/


	/* add & remove .active end манипуляции с бордером при hover*/
	var item = $('.list-item');

		$(item).mouseenter(function(){
			$(this).addClass('on-hover').prev().addClass('not-border');
		});
		$(item).mouseleave(function(){
			$(this).removeClass('on-hover').prev().removeClass('not-border');
		});

	$('.service-list .list-item').on('click', function(){
		var active = $('.list-item.active');
		active.removeClass('active');
	
		$(this).addClass('active').prev().addClass('not-border');

	});

	
	/* add & remove .active */


	/* mm-menu */
	if($("#menu").length){
		$("#menu").mmenu({
			"extensions": [
			"shadow-page",
			"listview-large",
			"fx-listitems-slide",
			"border-none"
			],
			"navbars": [
				{
					"position" 	: "top",
					"content" : [ 
						'<div class="box-logo"><a href="index.html" class="logo-link"><img src="img/logo.png" alt="logo"><span class="logo-text">anyService</span></a></div>'
						]
				},
				{
					"position": "bottom",
					"content": [
						"<p>&copy; 2017 anyService</p>"
					]
				}
			],
			"setSelected": {
				"hover": "true",
				"parent": "true"
			},

		}).on('click', 'a[href^="#/"]', function() {
				alert( "Thank you for clicking, but that's a demo link." );
				return false;
			}
		);
	};

	$('.js-btn-nav').click(function(){
		$('.btn-mobile').show();
	});
	

	$(document).mousedown(function (e){ // событие клика по веб-документу
		var btn = $('.btn-mobile');// тут указываем ID элемента
		
		if (!btn.is(e.target) // если клик был не по нашему блоку
			&& btn.has(e.target).length === 0  // и не по его дочерним элементам
			&& !$('.box-logo').is(e.target)
			&& !$('.mm-navbars-top').is(e.target)
			&& !$('.mm-navbar-size-1').is(e.target)
			&& !$('.mm-listview').is(e.target)
			&& !$('#mCSB_1').is(e.target)
			&& !$('.item_4').is(e.target)
			&& !$('p').is(e.target)
			&& !$('.mm-listview span').is(e.target)
			&& !$('a[href^="#/"]').is(e.target)
			) { btn.hide(); }  // скрываем его 
	});

	$(document).on('touchend', (function (e){ // тоже самое только для мобильных устройств
		var btn = $('.btn-mobile');

		if (!btn.is(e.target)
			&& btn.has(e.target).length === 0
			&& !$('.box-logo').is(e.target)
			&& !$('.mm-navbars-top').is(e.target)
			&& !$('.mm-navbar-size-1').is(e.target)
			&& !$('.mm-listview').is(e.target)
			&& !$('#mCSB_1').is(e.target)
			&& !$('.item_4').is(e.target)
			&& !$('p').is(e.target)
			&& !$('.mm-listview span').is(e.target)
			&& !$('a[href^="#/"]').is(e.target)
			) { btn.hide(); }
		})
	);

	/* mm-menu */
	

	/* Scrollbar */
	if($('.mm-panels').length) {
		$('.mm-panels').mCustomScrollbar({
			theme:"dark-thin",
			scrollInertia: 100
		});
	};
	/* Scrollbar */


});

var handler = function(){
	
	var height_footer = $('footer').height();	
	var height_header = $('header').height();		
	//$('.content').css({'padding-bottom':height_footer+40, 'padding-top':height_header+40});
	
	
	var viewport_wid = viewport().width;
	var viewport_height = viewport().height;
	
	if (viewport_wid <= 991) {
		
	}
	
}
$(window).bind('load', handler);
$(window).bind('resize', handler);



